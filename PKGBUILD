# Maintainer: Stefano Capitani <stefano_at_manjaro_dot_org>

pkgname=singularity-gdm-theme
_pkgname=singularity-gdm-theme
pkgver=20221212
_pkgrel=1
_theme=singularity-gnome-theme
_hook=gdm-theme-maia
_commit=6024943b4fea75242205f967f0dd2750c1c89dc3 #theme repo
_commit_gnome=8e644ec39b5ac5b3fd22ceff3a7c0f8d897aa1da  # master
pkgrel=1
pkgdesc="Manjaro GDM theme"
arch=(any)
url="https://gitlab.com/waser-technologies/images/$_theme"
license=('GPL3')
depends=(gnome-shell)
makedepends=('sassc' 'git')
conflicts=('singularity-gnome-maia-theme' 
		   "$_pkgname" 
		   "$_pkgname-dev" 
		   'singularity-gdm-theme-19.0' 
		   'singularity-gdm-theme-18.0')
replaces=('singularity-gnome-maia-theme'
		  'singularity-gdm-theme-19.0'
		  'singularity-gdm-theme-18.0')
source=("$_theme-$_commit.tar.gz::$url/-/archive/$_commit.tar.gz"
		'gnome-shell-theme.gresource.xml'
		"git+https://gitlab.gnome.org/GNOME/gnome-shell.git#commit=$_commit_gnome")
md5sums=('0812e9ff0973c78b13059f41c5b063bf'
         '70db7ac6d2b2ce93f4eea3a50ae3ca23'
         'SKIP')

pkgver() {
	date +%Y%m%d
}

prepare() {

#prepare build folder
	if [[ -d build ]]; then
		rm -R build
		mkdir -p build/theme
	else mkdir -p build/theme
	fi

workdir=$srcdir/gnome-shell/data

#move the correct files	
	cp -f $srcdir/*.xml	$srcdir/build/theme/

#generate css files
echo "Generating .css files from gitlab sources..."
cd $workdir/theme
sed -i '/selected_bg_color: if(\$variant /c\$selected_bg_color: if($variant == 'light', #16a085, #16a085);' gnome-shell-sass/_colors.scss
sed -i 's/#4a90d9/#16a085/' gnome-shell-sass/_high-contrast-colors.scss
sassc -a gnome-shell-high-contrast.scss gnome-shell-high-contrast.css
sassc -a gnome-shell.scss gnome-shell.css

# move the css files
echo "replacing the .css files with the new ones..."
cp -f $srcdir/gnome-shell/data/theme/*.css $srcdir/build/theme/
cp -f $srcdir/gnome-shell/data/theme/*.svg $srcdir/build/theme/
#change colors	
cd $srcdir/build/theme

echo "Adjusting svg colors..."
find . -type f -name '*.svg' -exec sed -i \
    "s/#2c1cff/#16a085/Ig;\
    s/#0000ff/#16a085/Ig;\
    s/#0b2e52/#244e37/Ig;\
    s/#1862af/#138971/Ig;\
    s/#3465a4/#3b7666/Ig;\
    s/#006098/#1f7a61/Ig;\
    s/#2b73cc/#17ab8e/Ig;\
    s/#3081e3/#24c891/Ig;\
    s/#15539e/#1b7a61/Ig" {} \;
    
#patch cancel gdm button
#echo "change background <cancel> button"
#cd $srcdir/build/theme
#find . -type f -name '*.css' -exec sed -i \
#    "s/#5b666a/#5f5f5f/Ig;\
#    s/#667478/#6f6f6f/Ig;\
#    s/#4f595d/#4f4f4f/Ig" {} \;
 
#compiling schemas
echo "compiling the files"
	cd $srcdir/build/theme 
	glib-compile-resources gnome-shell-theme.gresource.xml

}

package() {

	mkdir -p $pkgdir/usr/share/themes
	mkdir -p $pkgdir/usr/share/themes/$_pkgname
	cp -r $srcdir/build/theme/* $pkgdir/usr/share/themes/$_pkgname/
    
    cd $_theme-$_commit
    
    #Hook
    install -Dm644 $srcdir/$_theme-$_commit/$_hook-install.hook $pkgdir/usr/share/libalpm/hooks/$_hook-install.hook
    install -Dm755 $srcdir/$_theme-$_commit/$_hook-install.script $pkgdir/usr/share/libalpm/scripts/$_hook-install
    install -Dm644 $srcdir/$_theme-$_commit/$_hook-remove.hook $pkgdir/usr/share/libalpm/hooks/$_hook-remove.hook
    install -Dm755 $srcdir/$_theme-$_commit/$_hook-remove.script $pkgdir/usr/share/libalpm/scripts/$_hook-remove
    install -Dm644 $srcdir/$_theme-$_commit/gdm-theme-backup.hook $pkgdir/usr/share/libalpm/hooks/gdm-theme-backup.hook
    install -Dm755 $srcdir/$_theme-$_commit/gdm-theme-backup.script $pkgdir/usr/share/libalpm/scripts/gdm-theme-backup

}
